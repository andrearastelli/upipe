import json
import os


config_data = None


def readConfig():

    global config_data

    if config_data is not None:
        return config_data

    config_file = os.path.join('./', 'config.json')
    config_file = os.path.abspath(config_file)

    with open(config_file) as f:
        config_data = json.load(f)

    return config_data


def dbPath():
    config_data = readConfig()

    if not config_data['paths']['database']:
        return 'database.db'

    return config_data['paths']['database']


def acceptedGenericFormat(filename, fileFormat):
    config_data = readConfig()
    ext = os.path.splitext(filename)[1].replace('.', '')

    return ext.lower() in [format.lower() for format in config_data['settings']['file_formats'][fileFormat]]


def acceptedTextures(filename):
    return acceptedGenericFormat(filename, 'texture')


def acceptedFiles(filename):
    return acceptedGenericFormat(filename, 'file')


def generateGenericFilter(fileFormat):
    config_data = readConfig()

    file_formats = ['*.{}'.format(f.lower()) for f in config_data['settings']['file_formats'][fileFormat]]

    return ' '.join(file_formats)


def generateFileFormatFilter():
    return generateGenericFilter('file')


def generateTextureFormatFilter():
    return generateGenericFilter('texture')


if __name__ == '__main__':
    pass
