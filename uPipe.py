import sys

from PySide import QtGui
from uPipeUI import uPipeUI

if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)

    ui = uPipeUI()
    ui.show()

    app.exec_()
