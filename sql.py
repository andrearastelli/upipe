import os
import sqlite3
import configreader

connection = None


def connect(**kwargs):
    global connection

    if connection is not None:
        return connection

    # db_path = kwargs.get('dbPath', 'database.db')
    db_path = configreader.dbPath()
    connection = sqlite3.connect(db_path)

    connection.row_factory = sqlite3.Row

    return connection


def tableSetup():
    cursor = connect().cursor()
    cursor.execute('pragma foreign_keys = ON')
    cursor.execute('create table if not exists projects (id INTEGER PRIMARY KEY ASC AUTOINCREMENT, name, description)')
    cursor.execute('create table if not exists files (id INTEGER PRIMARY KEY ASC AUTOINCREMENT, projectId INTEGER, path, FOREIGN KEY(projectId) REFERENCES project(id) ON UPDATE CASCADE)')
    cursor.execute('create table if not exists textures (id INTEGER PRIMARY KEY ASC AUTOINCREMENT, fileId INTEGER, texture, FOREIGN KEY(fileId) REFERENCES files(id) ON UPDATE CASCADE)')


def dropTables():
    cursor = connect().cursor()
    cursor.execute('drop table projects')
    cursor.execute('drop table files')
    cursor.execute('drop table textures')


def addProject(projectName=None, projectDescription=None):
    if projectName is None:
        raise ValueError('Project name must not be None')

    if projectDescription is None:
        raise ValueError('Project description must not be None')

    cursor = connect().cursor()
    cursor.execute('insert into projects(name, description) values (?, ?)', (projectName, projectDescription))
    connect().commit()

    return cursor.lastrowid


def listProjects():
    cursor = connect().cursor()
    cursor.execute('select * from projects')
    return cursor


def addFile(projectId=None, path=None):
    if projectId is None:
        raise ValueError('No project id defined')

    if path is None:
        raise ValueError('No file path defined')

    cursor = connect().cursor()
    cursor.execute('insert into files(projectId, path) values (?, ?)', (projectId, path))
    connect().commit()

    return cursor.lastrowid


def addTexture(fileId=None, texture=None):
    if fileId is None:
        raise ValueError('No file id specified')

    if texture is None:
        raise ValueError('No texture path defined')

    cursor = connect().cursor()
    cursor.execute('insert into textures(fileId, texture) values (?, ?)', (fileId, texture))
    connect().commit()

    return cursor.lastrowid


def listFiles(projectId=None):
    if projectId is None:
        raise ValueError('No project id specified')

    cursor = connect().cursor()
    cursor.execute('select * from files where projectId = ?', (projectId, ))

    return cursor


def listTextures(fileId=None):
    if fileId is None:
        raise ValueError('No file id specified')

    cursor = connect().cursor()
    cursor.execute('select * from textures where fileId = ?', (fileId, ))

    return cursor


def listAllFiles():
    cursor = connect().cursor()
    cursor.execute('select * from files')

    return cursor


def listAllTextures():
    cursor = connect().cursor()
    cursor.execute('select * from textures')

    return cursor


def findFileByName(filename=None):
    cursor = connect().cursor()
    cursor.execute('select * from files where path like %?%', (filename, ))

    return cursor


if __name__ == '__main__':
    # addFile(1, 'test')
    # addFile(1, 'test/prova.mb')
    # addFile(1, 'test/prova/prova.mb')
    print listProjects().fetchall()
    print listAllFiles().fetchall()
    print listAllTextures().fetchall()
