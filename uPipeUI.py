from PySide import QtGui, QtCore
import sql
import configreader


class uPipeUI(QtGui.QWidget):

    def __init__(self):
        super(uPipeUI, self).__init__()

        self._fileUploadField = QtGui.QLineEdit()
        self._textureUploadField = QtGui.QLineEdit()

        self.setObjectName('uPipe')
        self.setWindowTitle('uPipe')
        self.initUI()
        self.setBaseSize(400, 300)

    def initUI(self):
        _mainLayout = QtGui.QVBoxLayout()

        _projectGroup = QtGui.QGroupBox('Project')
        _projectGroupLayout = QtGui.QFormLayout()
        _projectGroup.setLayout(_projectGroupLayout)

        # Project selector
        _projectLabel = QtGui.QLabel('Project:')
        _projectComboBox = QtGui.QComboBox()
        uPipeUI.fillProjectComboBox(_projectComboBox)

        _projectGroupLayout.addRow(_projectLabel, _projectComboBox)

        _mainLayout.addWidget(_projectGroup)

        # =======================================

        _fileUploadGroup = QtGui.QGroupBox('File upload')
        _fileUploadGroupLayout = QtGui.QFormLayout()
        _fileUploadGroup.setLayout(_fileUploadGroupLayout)

        _fileUploadLabel = QtGui.QLabel('File upload:')
        _textureUploadLabel = QtGui.QLabel('Texture upload:')

        # File upload
        _fileUploadLayout = QtGui.QHBoxLayout()
        _fileUploadSearch = QtGui.QPushButton('Search')
        _fileUploadSearch.clicked.connect(self.searchFileAction)
        self._fileUploadField.setPlaceholderText('path to the file to upload')
        _fileUploadLayout.addWidget(self._fileUploadField)
        _fileUploadLayout.addWidget(_fileUploadSearch)

        # Texture upload
        _textureUploadLayout = QtGui.QHBoxLayout()
        _textureUploadSearch = QtGui.QPushButton('Search')
        _textureUploadSearch.clicked.connect(self.searchTextureAction)
        self._textureUploadField.setPlaceholderText('path to the texture to upload')
        _textureUploadLayout.addWidget(self._textureUploadField)
        _textureUploadLayout.addWidget(_textureUploadSearch)

        _fileUploadGroupLayout.addRow(_fileUploadLabel, _fileUploadLayout)
        _fileUploadGroupLayout.addRow(_textureUploadLabel, _textureUploadLayout)

        _mainLayout.addWidget(_fileUploadGroup)

        # =======================================

        _actionLayout = QtGui.QHBoxLayout()

        # Action buttons
        _submit = QtGui.QPushButton('Submit')
        _reset = QtGui.QPushButton('Reset')
        _cancel = QtGui.QPushButton('Cancel')

        _submit.clicked.connect(self.submitAction)
        _reset.clicked.connect(self.resetAction)
        _cancel.clicked.connect(self.cancelAction)

        _actionLayout.addWidget(_reset)
        _actionLayout.addWidget(_cancel)
        _actionLayout.addWidget(_submit)

        _mainLayout.addLayout(_actionLayout)

        self.setLayout(_mainLayout)

    def submitAction(self):
        pass

    def resetAction(self):
        self._fileUploadField.setText('')
        self._textureUploadField.setText('')

    def cancelAction(self):
        QtCore.QCoreApplication.instance().quit()

    def searchFileAction(self):
        fileSelected = self.showFileDialog(configreader.generateFileFormatFilter())
        self._fileUploadField.setText(fileSelected[0])

    def searchTextureAction(self):
        fileSelected = self.showFileDialog(configreader.generateTextureFormatFilter())
        self._textureUploadField.setText(','.join(fileSelected))

    def showFileDialog(self, filter=None):
        fileDialog = QtGui.QFileDialog(self, 'File Dialog Caption')
        fileDialog.setDirectory('./')
        fileDialog.setFileMode(QtGui.QFileDialog.FileMode.ExistingFile)
        fileDialog.setAcceptMode(QtGui.QFileDialog.AcceptMode.AcceptOpen)
        fileDialog.setViewMode(QtGui.QFileDialog.ViewMode.Detail)
        fileDialog.setNameFilter(filter)
        if fileDialog.exec_():
            return fileDialog.selectedFiles()

    @classmethod
    def fillProjectComboBox(cls, projectComboBox):
        for project in sql.listProjects():
            projectComboBox.addItem(project[1])
